$( document ).ready(function() {

    console.log( "ready!" );
    var key = "gbgymt2016";
    var width = window.screen.width;
    var height = window.screen.height;

    getWorkoutList();
    $('#back_button_container').unbind('click');

    function getWorkoutList() {
      $.ajax({
          url: 'api/gymplanlist/1',
          type: 'GET',
          accepts: 'application/json',
          success: function(respone) {
            if(respone.status === true) {
              populateWorkoutList(respone.data);
              setWorkoutListEvents();
            }
            else {
              populateWorkoutListError(respone.message);
            }
          },
          error: function(xhr, status, error) {
            console.log('AJAX ERROR - workout list');
            console.log(xhr.statusText);
            console.log(status);
          }
      });
    }

    function populateWorkoutList(data) {
      var htmlString = '';
      for(var item in data) {
        htmlString += '<div class="gymlist_item" ' +
          'id="gymlist_item_' + data[item].id + '">' + data[item].name + '</div>';
      }
      $('#start_gymlist').html(htmlString);
    }

    function populateWorkoutListError(message) {
      $('#start_gymlist').html(message);
    }

    function setWorkoutListEvents() {
      $('.gymlist_item').unbind('click');
      $(".gymlist_item" ).click(function() {
        var idsplit = $(this).attr('id').split('_');
        var id = idsplit[2];
        openWorkoutSummary(id);
      });
    }

    function openWorkoutSummary(id) {
      $('.main_area').hide();
      $('#main_area_summary').show();
      $("#back_button_container").click(function() {
        $('.main_area').hide();
        $('#main_area_start').show();
        $('#back_button_container').unbind('click');
      });
      var workout = getWorkoutData(id, getWeekNum());
      workout = orderWorkoutData(workout);
      var test = 1;
      renderWorkout(workout);
    }

    function getWorkoutData(id, week) {
      return dummyWorkout;
    }

    function getWorkoutData2(id, week) {
      $.ajax({
          url: 'api/workout/' + id + '/' + week,
          type: 'GET',
          accepts: 'application/json',
          success: function(respone) {
            if(respone.status === true) {
              // Populate Object
              // Populate Elements
            }
            else {

            }
          },
          error: function(xhr, status, error) {
            console.log('AJAX ERROR - workout list');
            console.log(xhr.statusText);
            console.log(status);
          }
      });
    }

    function orderWorkoutData(workout) {
        var numOfExercises = workout.length;
        var originalWorkout = workout;
        var orderedWorkout = [];
        for(var item = 0 ; item < numOfExercises ; item ++) {
          // ORDER  = LATEST DATE - THEN BT EARLIEST TIME
          // get latest date
          var startDates = getStartDatesFrom(workout);
          var latestDate = getLatestDate(startDates);
          var index = getIndexOfEarliestTimeFromDate(workout, latestDate);
          orderedWorkout.push(originalWorkout[index]);
          workout.splice(index,1);
          // for all items with this date, get earlier time
          // romove, add to ordered array and
        }
        return orderedWorkout;
    }

    function getStartDatesFrom(workout) {
      var dates = [];
      for(var item in workout) {
        dates.push(workout[item].sets[0].starttime);
      }
      return dates;
    }
    function getLatestDate(dates) {
      var latestDate = null;
      for(var item in dates) {
        if(item === 0 || item === '0') {
          latestDate = dates[0];
        }
        else if(moment(latestDate).isAfter(dates[item < 1])) {
          latestDate = dates[item];
        }
      }
      return latestDate;
    }
    function getIndexOfEarliestTimeFromDate(workout, latestDate) {
      var index = null;
      var earliestTime = null;
      for(var item in workout) {
        var currentDate = moment(workout[item].sets[0].starttime);
        if(!earliestTime && moment(latestDate).isSame(currentDate, 'day')) {
          earliestTime = currentDate;
          index = item;
        }
        else if(moment(latestDate).isSame(currentDate, 'day') &&
                moment(earliestTime).isAfter(currentDate)) {
          earliestTime = currentDate;
          index = item;
        }
      }
      return index;
    }

    function getWeekNum() {
      var d = new Date();
      d.setHours(0,0,0);
      d.setDate(d.getDate() + 4 - (d.getDay() || 7));
      return Math.ceil((((d- new Date(d.getFullYear() , 0 , 1)) / 8.64e7) + 1) / 7) - 1;
    }

    function renderWorkout(workout) {
      console.log(workout);
      var htmlString = '';
      htmlString += getWorkoutInfo(workout);
      for(var item in workout) {
        htmlString += createSummaryItem(workout.workout[item], item, workout.round);
      }
      $('#main_area_summary').html(htmlString);
    }
    function getWorkoutInfo(workout) {
      var htmlString = '' +
            '<div id="summary_workoutinfo">' +
              '<div class="summary_workoutinfo_item">' +
                '<div class="summary_workoutinfo_item_label">Excerise:</div>' +
                '<div class="summary_workoutinfo_item_info" id="summary_workoutinfo_item_name">' + workout[0].routine + '</div>' +
              '</div>' +
              '<div class="summary_workoutinfo_item" id="summary_workoutinfo_item_condition">' +
                '<div class="summary_workoutinfo_item_label">Condition:</div>' +
                '<div class="summary_workoutinfo_item_info" id="summary_workoutinfo_item_condition">' + workout[0].sessionComment + '</div>' +
              '</div>' +
            '</div>';
        return htmlString;
    }

    function createSummaryItem(exercise, number, round) {
      number++;
      var htmlString = '<div class="summary_item">';
      htmlString += '<div class="summary_item_header">' + exercise.name + '</div>';
      htmlString += '<div class="summary_item_number">' + number + '</div>';
      htmlString += createSummaryCommentsList(exercise, round);
      htmlString += createRepsContainer(exercise, round);
      htmlString += '</div>';

      return htmlString;
    }

    function createSummaryCommentsList(exercise, round) {
      var time = moment(exercise.session.date);
      var hoursmin = time.format('hhmm');
      var day = time.format('dddd');
      var date = time.format('DD-MM-YYYY');
      var when = 'Round ' + round + ' / ' + hoursmin + ' / ' + day + ' / ' + date;
      var htmlString = '' +
            '<div class="summary_item_comments_list">' +
              '<div class="summary_item_comments">' +
                '<div class="summary_item_comments_label">When:</div>' +
                '<div class="summary_item_comments_info">' + when + '</div><br>' +
              '</div>' +
              '<div class="summary_item_comments">' +
                '<div class="summary_item_comments_label">Comment:</div>' +
                '<div class="summary_item_comments_info">' + exercise.session.comment + '</div><br>' +
              '</div>' +
            '</div>';
      return htmlString;
    }

    function createRepsContainer(exercise) {
      var htmlString = '<div class="summar_rep_rows_container">';
      htmlString += createSummaryRepHeader();
      for(var item in exercise.session.sets) {
        htmlString += createSummaryRepRow(exercise.session.sets[item]);
      }
      htmlString += '</div>';
      return htmlString;
    }

    function createSummaryRepHeader() {
      var htmlString = '' +
            '<div class="summary_rep_row_header">' +
              '<div class="summary_rep_row_item">Weight</div>' +
              '<div class="summary_rep_row_item summary_rep_row_item_reps">Reps</div>' +
              '<div class="summary_rep_row_item">Next W</div>' +
              '<div class="summary_rep_row_item summary_rep_row_item_comment">Comment</div>' +
              '<div class="summary_rep_row_item">Break</div>' +
              '<div class="summary_rep_row_item">Duration</div>' +
            '</div><br>';
      return htmlString;
    }

    function createSummaryRepRow(set) {
      var breakTime =  calcBreakTime(set.start);
      var duration = calcDurationTime(set.start, set.finish);

      var htmlString = '' +
            '<div class="summary_rep_row">' +
              '<div class="summary_rep_row_item">' + set.weight + '</div>' +
              '<div class="summary_rep_row_item summary_rep_row_item_reps">' + set.reps + '</div>' +
              '<div class="summary_rep_row_item">' + set.nextWeight + '</div>' +
              '<div class="summary_rep_row_item summary_rep_row_item_comment">' + set.comment + '</div>' +
              '<div class="summary_rep_row_item">' + breakTime + '</div>' +
              '<div class="summary_rep_row_item">' + duration + '</div>' +
            '</div><br>';
      return htmlString;
    }

    function calcBreakTime() {

    }

    function calcDurationTime(start, finish) {
      start = moment(start);
      if(set.finish !== null) {
          finish = moment(set.finish);
          return moment(finish.diff(start)).format('mm:ss');
      }
      return '-';
    }





















});

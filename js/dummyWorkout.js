var workout =
{

  "id": 1,
  "round": 1,
  "name": "chest",
  "date": "2011-04-11T11:51:00",
  "comment":"tired tonight",
  "exercises": [
    {
      "exericseId": 1,
      "name": "Chest Press",
      "session": {
        "date": "2011-04-11T11:51:00",
        "comment": "technique, don't increase",
        "order": 1,
        "sets": [
          {
            "order": 1,
            "weight": 20.00,
            "nextWeight": 25.00,
            "reps": 10,
            "comment": "TOUGHT-T",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 2,
            "weight": 20.00,
            "nextWeight": 25.00,
            "reps": 8,
            "comment": "TOUGHT-T",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 3,
            "weight": 20.00,
            "nextWeight": 25.00,
            "reps": 6,
            "comment": "TOUGHT-T",
            "start": "2011-04-11T11:51:00",
            "finish": "2011-04-11T11:52:43"
          }
        ]
      }
    },
    {
      "exericseId": 2,
      "name": "Chest-Fly",
      "session": {
        "date": "2011-04-11T11:51:00",
        "comment": "technique, don't increase",
        "order": 2,
        "sets": [
          {
            "order": 1,
            "weight": 10.00,
            "nextWeight": 15.00,
            "reps": 10,
            "comment": "EASY",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 2,
            "weight": 10.00,
            "nextWeight": 15.00,
            "reps": 8,
            "comment": "EASY",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 3,
            "weight": 10.00,
            "nextWeight": 51.00,
            "reps": 6,
            "comment": "EASY",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          }
        ]
      }
    }
  ]
};

var dummyWorkout = [ // Array of exercises = GET most previous exercises // App will figure out rest
  {
    id: 1,
    type: 3,
    name: 'Chess Press Declined',
    originalOrder: 1,
    routine: 'Chest',
    date: '2015-02-22T19:30:00',
    comment: 'need to practise technique',
    sessionComment: 'tired, flu', // server can get this from DB session table related to Ex
    sets: [
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: 'TOUGHT-T',
        starttime: '2015-02-22T19:30:00',
        finishtime: null,
      },
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: "TOUGHT-T",
        starttime: '2015-02-22T19:32:00',
        finishtime: null,
      },
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: "TOUGHT-T",
        starttime: '2015-02-22T19:34:00',
        finishtime: null
      }
    ]
  },
  {
    id: 2,
    type: 1,
    name: 'Chess Press',
    originalOrder: 2,
    date: '2015-02-22T19:37:00',
    comment: 'need to practise technique',
    sessionComment: 'tired, flu', // server can get this from DB session table related to Ex
    sets: [
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: 'TOUGHT-T',
        starttime: '2015-02-22T19:37:00',
        finishtime: null,
      },
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: "TOUGHT-T",
        starttime: '2015-02-22T19:39:00',
        finishtime: null,
      },
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: "TOUGHT-T",
        starttime: '2015-02-22T19:41:00',
        finishtime: null
      }
    ]
  },
  {
    id: 3,
    type: 5,
    name: 'Chess Inclined',
    originalOrder: 3,
    date: '2015-02-17T19:37:00',
    comment: 'need to practise technique',
    sessionComment: 'tired, flu', // server can get this from DB session table related to Ex
    sets: [
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: 'TOUGHT-T',
        starttime: '2015-02-17T19:36:00',
        finishtime: null,
      },
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: "TOUGHT-T",
        starttime: '2015-02-17T19:39:00',
        finishtime: null,
      },
      {
        weight: 20.00,
        nextWeight: 25.00,
        reps: 10,
        comment: "TOUGHT-T",
        starttime: '2015-02-17T19:41:00',
        finishtime: null
      }
    ]
  }
];



/*
var session =
{   /// THIS COULD BE ADDED TO THE GYMPLAN OBJECFROM START -- AND MADE GLOBAL
  "id": 1,
  "round": 1, // DOESN'T HAVE TO STORED IN DB??
  //"name": "chest", // NO NEED FOR NAME == SHOULD BE SELECTED FROM HOMEPAGE
  "date": "2011-04-11T11:51:00", // first rep would also give date?? what if no reps done?
  "comment":"tired tonight",
  "exercises": [
    {
      "exericseId": 1,
      // ADD TYPE that includes name   //"name": "Chest Press",
      // also date, comment, order
      // ARRAY OF SETS
      "session": {
        "date": "2011-04-11T11:51:00",
        "comment": "technique, don't increase",
        "order": 1,
        "sets": [
          {
            "order": 1,
            "weight": 20.00,
            "nextWeight": 25.00,
            "reps": 10,
            "comment": "TOUGHT-T",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 2,
            "weight": 20.00,
            "nextWeight": 25.00,
            "reps": 8,
            "comment": "TOUGHT-T",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 3,
            "weight": 20.00,
            "nextWeight": 25.00,
            "reps": 6,
            "comment": "TOUGHT-T",
            "start": "2011-04-11T11:51:00",
            "finish": "2011-04-11T11:52:43"
          }
        ]
      }
    },
    {
      "exericseId": 2,
      "name": "Chest-Fly",
      "session": {
        "date": "2011-04-11T11:51:00",
        "comment": "technique, don't increase",
        "order": 2,
        "sets": [
          {
            "order": 1,
            "weight": 10.00,
            "nextWeight": 15.00,
            "reps": 10,
            "comment": "EASY",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 2,
            "weight": 10.00,
            "nextWeight": 15.00,
            "reps": 8,
            "comment": "EASY",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          },
          {
            "order": 3,
            "weight": 10.00,
            "nextWeight": 51.00,
            "reps": 6,
            "comment": "EASY",
            "start": "2011-04-11T11:51:00",
            "finish": null,
          }
        ]
      }
    }
  ]
};
*/

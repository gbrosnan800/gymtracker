<?php

$fullURI = (string) $_SERVER['REQUEST_URI'];
$fullURI = formatURI();
$service = explode('/', $fullURI)[3];



	function formatURI() {
		$uri = (string) $_SERVER['REQUEST_URI'];
		if (strpos($uri,'/btsfitness/localhost') !== false) {
			$uri = str_replace("/btsfitness/localhost", "", $uri);
			return $uri;
		}
		return $uri;
	}

	if($service == 'gymplanlist' && $_SERVER['REQUEST_METHOD'] == 'GET') {
		if(!getParam(4)) {
			returnErrorMessage('GymPlanList: Needs an extra URI Parameter');
		}
		else if(!getParam(5)) {
			getGymPlanList(getParam(4));
		}
		else if(!getParam(6)) {
			returnErrorMessage('GymPlanList: Too many URI parameters');
		}
	}
	else {
			returnErrorMessage('service not found');
	}

	function getParam($number) {
		$fullURI = formatURI();
		$uriSplit = explode('/', $fullURI);
		if(sizeof($uriSplit) > $number) {
			return explode('/', $fullURI)[$number];
		}
		else {
			return false;
		}
	}

	function returnErrorMessage($message) {
		header("Content-type: application/json");
		echo '{"status":false, "message": "' . $message . '"}';
	}

	function returnJsonData($data) {
		header("Content-type: application/json");
		echo '{"status":true, "data": ' . $data . '}';
	}

	function authenticateKey($jsonData) {
		if(isset( $jsonData['key'])) {
			if($jsonData['key'] == 'gbgymt2016') {
				return true;
			}
			else {
				returnErrorMessage('Authentication Key incorrect');
				return false;
			}
		}
		returnErrorMessage('Authentication Key missing');
		return false;
	}

	function getDBConfig() {
		$config = require 'config.php';
		echo $config;;
	}

	function getGymPlanList($workout_num) {
		try {
			$dbconfig = require 'config.php';
			$link = mysql_connect($dbconfig['db_host'], $dbconfig['db_user'], $dbconfig['db_password']);
			mysql_select_db($dbconfig['db_name']);
			$sql = "select * from routine where gymplan_id = " . $workout_num;
			$result = mysql_query($sql);
			$data = array();
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				array_push($data, $row);
			}
			mysql_close($link);
			returnJsonData(json_encode($data));
		}
		catch(Exception $e) {
			returnErrorMessage($e->getMessage());
		}
	}

	function postWorkout() {
		$workout = file_get_contents('php://input');
		$jsonData = json_decode($workout, true);
		if(authenticateKey($jsonData)) {
			returnErrorMessage('Workout Post not available yet');
		}
	}

	function GymPlanList($week) {

		$dummyWorkoutJson = createDummyWorkoutJson();

		returnErrorMessage($dummyWorkoutJson);
	}

	function createDummyWorkoutJson() {

		// would be too large, do real instead - example in dummyWorkout.json
		// instead of getting previous week, get most recent workout - could be 2wks+ ago
		// two tables for workout? one for desc, and one for every event? with comment and date

		return 'not avaible';
	}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Gerry Brosnan">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'>
    <title>GYM-Tracker</title>
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/moment.min.js"></script>
    <script src="js/dummyWorkout0.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>

    <div id="navigation">
      <div id="back_button_container"><div id="back_button"></div></div>
      <div id="logo">GYM-<span class="font_italic">Tracker</span></div>
    </div>


    <div class="main_area" id="main_area_start">
      <div id="start_gymlist"></div>
    </div>

    <div class="main_area" id="main_area_summary">
      <div id="summary_workoutinfo">
        <div class="summary_workoutinfo_item">
          <div class="summary_workoutinfo_item_label">Excerise:</div>
          <div class="summary_workoutinfo_item_info" id="summary_workoutinfo_item_name">Chest</div>
        </div>
        <div class="summary_workoutinfo_item" id="summary_workoutinfo_item_condition">
          <div class="summary_workoutinfo_item_label">Condition:</div>
          <div class="summary_workoutinfo_item_info" id="summary_workoutinfo_item_condition">Tired, stressed</div>
        </div>
      </div>
      <div class="summary_item">
        <div class="summary_item_header">Barbell Bicep Curl</div>
        <div class="summary_item_number">2</div>
        <div class="summary_item_comments_list">
          <div class="summary_item_comments">
            <div class="summary_item_comments_label">When:</div>
            <div class="summary_item_comments_info">Round 1 / 1920 / Tuesday / 2-Feb-2016</div><br>
          </div>
          <div class="summary_item_comments">
            <div class="summary_item_comments_label">Comment:</div>
            <div class="summary_item_comments_info">Tough, improve on technique</div><br>
          </div>
        </div>
        <div class="summar_rep_rows_container">
          <div class="summary_rep_row summary_rep_row_header">
            <div class="summary_rep_row_item">Weight</div>
            <div class="summary_rep_row_item summary_rep_row_item_reps">Reps</div>
            <div class="summary_rep_row_item">Next W</div>
            <div class="summary_rep_row_item summary_rep_row_item_comment" >Comment</div>
            <div class="summary_rep_row_item">Break</div>
            <div class="summary_rep_row_item">Duration</div>
          </div><br>
          <div class="summary_rep_row">
            <div class="summary_rep_row_item">20.00</div>
            <div class="summary_rep_row_item summary_rep_row_item_reps">10</div>
            <div class="summary_rep_row_item">25.00</div>
            <div class="summary_rep_row_item summary_rep_row_item_comment">TOUGHT-T</div>
            <div class="summary_rep_row_item">2.00</div>
            <div class="summary_rep_row_item">2.00</div>
          </div><br>
          <div class="summary_rep_row">
            <div class="summary_rep_row_item">20.00</div>
            <div class="summary_rep_row_item summary_rep_row_item_reps">8</div>
            <div class="summary_rep_row_item">25.00</div>
            <div class="summary_rep_row_item summary_rep_row_item_comment">TOUGHT-T</div>
            <div class="summary_rep_row_item">2.00</div>
            <div class="summary_rep_row_item">2.00</div>
          </div><br>
          <div class="summary_rep_row">
            <div class="summary_rep_row_item">20.00</div>
            <div class="summary_rep_row_item summary_rep_row_item_reps">6</div>
            <div class="summary_rep_row_item">25.00</div>
            <div class="summary_rep_row_item summary_rep_row_item_comment">TOUGHT-T</div>
            <div class="summary_rep_row_item">2.00</div>
            <div class="summary_rep_row_item">2.00</div>
          </div>
        </div>

      </div>





    </div>

    <div class="main_area" id="main_area_exercise">

      <div class="font_underline" id="ex_header">04 - Barbell Shoulder Press</div>

      <div class="ex_rep_container">
        <div class="ex_rep_tophalf">
          <div class="ex_rep_tophalf_item ex_rep_name font_underline">REP 1</div>
          <div class="ex_rep_tophalf_item ex_rep_start">START</div>
          <div class="ex_rep_tophalf_item ex_rep_stop">STOP</div>
        </div>
        <div class="ex_rep_bottomhalf">
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">20</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_decimal">.</div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">00</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_data_container_repcount"><div class="ex_data_up"></div><div class="ex_data_input">10</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">25</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_decimal">.</div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">00</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_data_container_comment"><div class="ex_data_up"></div><div class="ex_data_input ex_data_input_comment">TOUGH-T</div><div class="ex_data_down"></div></div>
        </div>
      </div>

      <div class="ex_rep_container">
        <div class="ex_rep_tophalf">
          <div class="ex_rep_tophalf_item ex_rep_name font_underline">REP 1</div>
          <div class="ex_rep_tophalf_item ex_rep_start">START</div>
          <div class="ex_rep_tophalf_item ex_rep_stop">STOP</div>
        </div>
        <div class="ex_rep_bottomhalf">
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">20</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_decimal">.</div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">00</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_data_container_repcount"><div class="ex_data_up"></div><div class="ex_data_input">10</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">25</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_decimal">.</div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">00</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_data_container_comment"><div class="ex_data_up"></div><div class="ex_data_input ex_data_input_comment">TOUGH-T</div><div class="ex_data_down"></div></div>
        </div>
      </div>

      <div class="ex_rep_container">
        <div class="ex_rep_tophalf">
          <div class="ex_rep_tophalf_item ex_rep_name font_underline">REP 1</div>
          <div class="ex_rep_tophalf_item ex_rep_start">START</div>
          <div class="ex_rep_tophalf_item ex_rep_stop">STOP</div>
        </div>
        <div class="ex_rep_bottomhalf">
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">20</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_decimal">.</div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">00</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_data_container_repcount"><div class="ex_data_up"></div><div class="ex_data_input">10</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">25</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_decimal">.</div>
          <div class="ex_data_container"><div class="ex_data_up"></div><div class="ex_data_input">00</div><div class="ex_data_down"></div></div>
          <div class="ex_data_container ex_data_container_comment"><div class="ex_data_up"></div><div class="ex_data_input ex_data_input_comment">TOUGH-T</div><div class="ex_data_down"></div></div>
        </div>
      </div>

      <div class="ex_comments">
        <div class="ex_comments_item">
          <div class="ex_comments_label">Order:</div>
          <div class="ex_comments_info">1</div><br>
        </div>
        <div class="ex_comments_item">
          <div class="ex_comments_label">Comment:</div>
          <div class="ex_comments_info">Tough, improve on technique</div><br>
        </div>
      </div>

    </div>

    <div id="comment">
      <div id="comment_header">COMMENT:</div>
      <textarea></textarea>
      <div class="commentButton" id="saveComment">Save</div>
      <div class="commentButton" id="canceComment">Cancel</div>
    </div>
  </body>
</html>
